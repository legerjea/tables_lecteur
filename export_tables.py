#!/usr/bin/env python3

import os
import sys
import gzip
import json
import numpy as np
import scipy.stats as sps


SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(SCRIPT_DIR, "tables"))

from tables.tables_gen import (
    NORMCDF_XLIM,
    BINOM_N,
    BINOM_PS,
    POISSON_XLIM,
    POISSON_LAMBDA,
    STUDENT_ALPHAS,
    STUDENT_DF,
    CHISQ_DF,
    CHISQ_ALPHAS,
    FISHER_DF1,
    FISHER_DF2,
    FISHER_ALPHAS,
    reformat,
    notaprob2,
    finv,
    tinv,
    chisqinv,
)
from tables_lecteur._tableobj import (
    ListeFamillesTables,
    FamilleTables,
    Table,
    TableParametre,
    TableValue,
    TableExpr,
)


def export_table_fisher():
    nus1, nus2 = np.unique(np.concatenate(FISHER_DF1)), FISHER_DF2
    return FamilleTables(
        nom="Fractiles des lois de Fisher",
        proprietes=(
            TableExpr(
                "∀ν<sub>1</sub>,ν<sub>2</sub>, ∀α, 𝓯<sub>ν<sub>1</sub>,ν<sub>2</sub>;1-α</sub>=1/𝓯<sub>ν<sub>2</sub>,ν<sub>1</sub>;α</sub>",
                r"\forall\nu_1,\nu_2,\,\forall\alpha,\; f_{\nu_1,\nu_2;1-\alpha}=1/f_{\nu_1,\nu_2;\alpha}",
            ),
        ),
        variable=TableExpr("α", r"\alpha"),
        resultat=TableExpr(
            "𝓯<sub>ν<sub>1</sub>,ν<sub>2</sub>;α</sub>", r"f_{\nu_1,\nu_2;\alpha}"
        ),
        resultatalias=TableExpr(
            "F<sup>-1</sup><sub>𝓕<sub>ν<sub>1</sub>,ν<sub>2</sub></sub></sub>(α)",
            r"F_{\mathcal F_{\nu_1,\nu_2}}^{-1}(\alpha)",
        ),
        parametres=(
            TableParametre(
                TableExpr("ν<sub>1</sub>", r"\nu_1"),
                [
                    TableValue(np.inf, "+∞", r"+\infty")
                    if np.isinf(nu)
                    else TableValue(nu_int := int(nu), f"{nu_int}", f"{nu_int}")
                    for nu in nus1
                ],
            ),
            TableParametre(
                TableExpr("ν<sub>2</sub>", r"\nu_2"),
                [
                    TableValue(np.inf, "+∞", r"+\infty")
                    if np.isinf(nu)
                    else TableValue(nu, f"{nu}", f"{nu}")
                    for nu in nus2
                ],
            ),
        ),
        tables={
            (nu1, nu2): Table(
                xstart=TableValue(0.5, "0.5", "0.5"),
                xstop=TableValue(1, "1", "1"),
                increasing=True,
                xvalues=[TableValue(0, "0", "0")]
                + [
                    TableValue(alpha, x2 := f"{alpha:.3f}", x2)
                    for alpha in FISHER_ALPHAS
                ]
                + [TableValue(1, "1", "1")],
                yvalues=[
                    TableValue(1, reformat(1), reformat(1))
                    if np.isinf(nu1) and np.isinf(nu2)
                    else TableValue(0, "0", "0")
                ]
                + [
                    TableValue(y := finv(alpha, nu1, nu2), z := reformat(y), z)
                    for alpha in FISHER_ALPHAS
                ]
                + [
                    TableValue(1, reformat(1), reformat(1))
                    if np.isinf(nu1) and np.isinf(nu2)
                    else TableValue(np.inf, "+∞", r"+\infty")
                ],
            )
            for nu1 in nus1
            for nu2 in nus2
        },
    )


def export_table_binom():
    N, ps = BINOM_N, BINOM_PS
    values_N = list(range(2, N + 1))
    return FamilleTables(
        nom="Fonction de répartition de la loi binomiale",
        proprietes=(
            TableExpr(
                "Si X∼𝓑(n,p), alors (n-X)∼𝓑(n,1-p)",
                r"\text{Si }X\sim\mathcal B(n,p),\text{ alors }n-X\sim\mathcal B(n,1-p)",
            ),
        ),
        variable=TableExpr("x", "x"),
        resultat=TableExpr("F<sub>𝓑(n,p)</sub>(x)", r"F_{\mathcal B(n,p)}(x)"),
        resultatalias=TableExpr(
            "ℙ(X≤x), avec X∼𝓑(n,p)",
            r"\mathcal P(X\leq x),\text{ avec }X\sim\mathcal B(n,p)",
        ),
        parametres=(
            TableParametre(
                TableExpr("n", "n"),
                [TableValue(n, f"{n:d}", f"{n:d}") for n in values_N],
            ),
            TableParametre(
                TableExpr("p", "p"), [TableValue(p, f"{p:.2f}", f"{p:.2f}") for p in ps]
            ),
        ),
        tables={
            (n, p): Table(
                xstart=TableValue(0, "0", "0"),
                xstop=TableValue(n, f"{n:d}", f"{n:d}"),
                increasing=True,
                xvalues=[TableValue(x, f"{x:d}", f"{x:d}") for x in range(n + 1)],
                yvalues=[
                    TableValue(y := sps.binom.cdf(x, n, p), *notaprob2(y))
                    for x in range(n + 1)
                ],
            )
            for n in values_N
            for p in ps
        },
    )


def export_table_poisson():
    lambdas = np.unique(np.concatenate(POISSON_LAMBDA))
    xstop = {l: xs for (ls, xs) in zip(POISSON_LAMBDA, POISSON_XLIM) for l in ls}
    return FamilleTables(
        nom="Fonction de répartition de la loi de Poisson",
        variable=TableExpr("x", "x"),
        resultat=TableExpr("F<sub>𝓟(λ)</sub>(x)", r"F_{\mathcal P(\lambda)}(x)"),
        resultatalias=TableExpr(
            "ℙ(X≤x), avec X∼𝓟(λ)",
            r"\mathcal P(X\leq x),\text{ avec }X\sim\mathcal P(\lambda)",
        ),
        parametres=(
            TableParametre(
                TableExpr("λ", r"\lambda"),
                [TableValue(l, f"{l:.1f}", f"{l:.1f}") for l in lambdas],
            ),
        ),
        tables={
            (l,): Table(
                xstart=TableValue(0, "0", "0"),
                xstop=TableValue(xs := xstop[l] - 1, f"{xs:d}", f"{xs:d}"),
                increasing=True,
                xvalues=[TableValue(x, f"{x:d}", f"{x:d}") for x in range(xstop[l])],
                yvalues=[
                    TableValue(y := sps.poisson.cdf(x, l), *notaprob2(y))
                    for x in range(xstop[l])
                ],
            )
            for l in lambdas
        },
    )


def export_table_normcdf():
    xlim = NORMCDF_XLIM
    xvalues = np.arange(0, xlim, 0.01)
    return FamilleTables(
        nom="Fonction de répartition de la loi normale centrée réduite",
        proprietes=(TableExpr("∀x,  Φ(-x)=1-Φ(x)", r"\forall x,\;\Phi(-x)=1-\Phi(x)"),),
        variable=TableExpr("x", "x"),
        resultat=TableExpr("Φ(x)", r"\Phi(x)"),
        parametres=(),
        tables={
            (): Table(
                xstart=TableValue(0.0, "0", "0"),
                xstop=TableValue(xlim, f"{xlim}", f"{xlim}"),
                increasing=True,
                xvalues=[TableValue(x, x2 := f"{x:.2f}", x2) for x in xvalues]
                + [TableValue(np.inf, "+∞", r"+\infty")],
                yvalues=[
                    TableValue(y := sps.norm.cdf(x), *notaprob2(y)) for x in xvalues
                ]
                + [TableValue(1, "1", "1")],
            )
        },
    )


def export_table_normppf():
    xvalues = np.arange(0.5, 1, 0.001)
    return FamilleTables(
        nom="Fractiles de la loi normale centrée réduite",
        proprietes=(
            TableExpr(
                "∀α,  u<sub>1-α</sub>=-u<sub>α</sub>",
                r"\forall\alpha,\; u_{1-\alpha}=-u_\alpha",
            ),
        ),
        variable=TableExpr("α", r"\alpha"),
        resultat=TableExpr("u<sub>α</sub>", r"u_\alpha"),
        resultatalias=TableExpr("Φ<sup>-1</sup>(α)", r"\Phi^{-1}(\alpha)"),
        parametres=(),
        tables={
            (): Table(
                xstart=TableValue(0.5, "0.5", "0.5"),
                xstop=TableValue(1, "1", "1"),
                increasing=True,
                xvalues=[TableValue(x, x2 := f"{x:.3f}", x2) for x in xvalues]
                + [TableValue(1, "1", "1")],
                yvalues=[
                    TableValue(y := sps.norm.ppf(x), z := reformat(y, 4), z)
                    for x in xvalues
                ]
                + [TableValue(np.inf, "+∞", r"+\infty")],
            )
        },
    )


def export_table_student():
    return FamilleTables(
        nom="Fractiles des lois de Student",
        proprietes=(
            TableExpr(
                "∀ν, ∀α,  t<sub>ν;1-α</sub>=-t<sub>ν;α</sub>",
                r"\forall\nu,\,\forall\alpha,\; t_{\nu;1-\alpha}=-t_{\nu;\alpha}",
            ),
        ),
        variable=TableExpr("α", r"\alpha"),
        resultat=TableExpr("t<sub>ν;α</sub>", r"t_{\nu;\alpha}"),
        resultatalias=TableExpr(
            "F<sup>-1</sup><sub>𝓣<sub>ν</sub></sub>(α)",
            r"F_{\mathcal T_\nu}^{-1}(\alpha)",
        ),
        parametres=(
            TableParametre(
                TableExpr("ν", r"\nu"),
                [
                    TableValue(np.inf, "+∞", r"+\infty")
                    if np.isinf(nu)
                    else TableValue(nu, f"{nu}", f"{nu}")
                    for nu in STUDENT_DF
                ],
            ),
        ),
        tables={
            (nu,): Table(
                xstart=TableValue(0.5, "0.5", "0.5"),
                xstop=TableValue(1, "1", "1"),
                increasing=True,
                xvalues=[
                    TableValue(alpha, x2 := f"{alpha:.4f}", x2)
                    for alpha in STUDENT_ALPHAS
                ]
                + [TableValue(1, "1", "1")],
                yvalues=[
                    TableValue(y := tinv(alpha, nu), z := reformat(y), z)
                    for alpha in STUDENT_ALPHAS
                ]
                + [TableValue(np.inf, "+∞", r"+\infty")],
            )
            for nu in STUDENT_DF
        },
    )


def export_table_chisq():
    return FamilleTables(
        nom="Fractiles des lois du χ²",
        variable=TableExpr("α", r"\alpha"),
        resultat=TableExpr("χ²<sub>ν;α</sub>", r"\chi^2_{\nu;\alpha}"),
        resultatalias=TableExpr(
            "F<sup>-1</sup><sub>χ<sup>2</sub><sub>ν</sub></sub>(α)",
            r"F_{\chi^2_\nu}^{-1}(\alpha)",
        ),
        parametres=(
            TableParametre(
                TableExpr("ν", r"\nu"),
                [
                    TableValue(np.inf, "+∞", r"+\infty")
                    if np.isinf(nu)
                    else TableValue(nu, f"{nu}", f"{nu}")
                    for nu in CHISQ_DF
                ],
            ),
        ),
        tables={
            (nu,): Table(
                xstart=TableValue(0, "0", "0"),
                xstop=TableValue(1, "1", "1"),
                increasing=True,
                xvalues=[TableValue(0, "0", "0")]
                + [
                    TableValue(alpha, x2 := f"{alpha:.4f}", x2)
                    for alpha in CHISQ_ALPHAS
                ]
                + [TableValue(1, "1", "1")],
                yvalues=[TableValue(0, "0", r"0")]
                + [
                    TableValue(y := chisqinv(alpha, nu), z := reformat(y), z)
                    for alpha in CHISQ_ALPHAS
                ]
                + [TableValue(np.inf, "+∞", r"+\infty")],
            )
            for nu in CHISQ_DF
        },
    )


if __name__ == "__main__":

    familles = ListeFamillesTables(
        [
            export_table_normcdf(),
            export_table_normppf(),
            export_table_binom(),
            export_table_poisson(),
            export_table_chisq(),
            export_table_student(),
            export_table_fisher(),
        ]
    )

    try:
        os.mkdir(os.path.join(SCRIPT_DIR, "tables_lecteur", "data"))
    except FileExistsError:
        pass
    with open(os.path.join(SCRIPT_DIR, "tables_lecteur", "data", "all.json"), "w") as f:
        familles.dump(f)
