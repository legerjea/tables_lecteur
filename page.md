---
title: Programme de visualisation des tables
author: Jean-Benoist Leger, Université de technologie de Compiègne
---

## Préambule

Ce programme permet de lire et de chercher des valeurs dans les tables de SY02.
Les informations retournées sont strictement équivalentes aux tables (si ce
n'est pas le cas, il s'agit d'un bug à faire remonter).

_Disclamer_: Le développement d'application graphique n'est pas dans les
compétances de l'enseignant-chercheur qui l'a développé.

Merci de retourner tout bug et toute proposition d'amélioration [ici][issues].
Les _merge requests_ sont également bienvenues sur le [code source][depot] ou
pour modifier et compléter cette page sur le [dépot][depot].

[issues]: https://gitlab.utc.fr/legerjea/tables_lecteur/-/issues
[depot]: https://gitlab.utc.fr/legerjea/tables_lecteur

## Procédure d'installation

### Sous Linux

Installer PyQt5 avec votre gestionnaire de paquets. Sous Debian et dérivées
(dont Ubuntu), dans un terminal (cette étape n'est pas nécessaire pour une mise
à jour du programme):

```bash
sudo apt install python3-pyqt5
```

Puis:

```bash
pip install --upgrade tables-lecteur --extra-index-url https://gitlab.utc.fr/api/v4/projects/10567/packages/pypi/simple
```

Pour lancer le programme, utiliser la commande `tables-lecteur` dans un terminal
ou dans votre lanceur d'application.

### Sous MacOS

Installer PyQt5 avec `brew`. Dans un terminal (cette étape n'est pas nécessaire
pour une mise à jour du programme):

```bash
brew install pyqt@5
```

Puis:

```bash
pip install --upgrade tables-lecteur --extra-index-url https://gitlab.utc.fr/api/v4/projects/10567/packages/pypi/simple
```

Pour lancer le programme, utiliser la commande `tables-lecteur` dans un terminal
ou dans votre lanceur d'application.

### Sous Windows

_Proposition de documentation, non testée. Les propositions sont bienvenues sur [le dépot][depot]._

Installer python à partir du _Microsoft store_. Puis, au sein de l'invite de
commande, installer `PyQt5`:

```
pip install PyQt5
```

Puis:

```bash
pip install --upgrade tables-lecteur --extra-index-url https://gitlab.utc.fr/api/v4/projects/10567/packages/pypi/simple
```

Pour lancer le programme, utiliser la commande `tables-lecteur` dans l'invite de
commande ou dans votre lanceur d'application.

## Licence

 - Manipulation de tables (`tables_lecteur/_tableobj.py` et `tables_lecteur/_data.py`): licence MIT.
 - Interface graphique (`tables_lecteur/_gui.py`, `table_lecteur/__init__.py`): GPLv3 (utilisation de PyQt en GPLv3).
 - Documents (dont cette page): [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).
