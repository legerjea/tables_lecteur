# Copyright (C) 2022, Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Lecteur de tables statistiques utilisées en SY02
"""

__title__ = "tables_lecteur"
__author__ = "Jean-Benoist Leger"
__licence__ = "GPLv3"

version_info = (0, 1)
__version__ = ".".join(map(str, version_info))

from ._data import Familles
from ._tableobj import HorsDeLaTableError
