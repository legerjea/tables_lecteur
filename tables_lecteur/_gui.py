# Copyright (C) 2022, Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"Internal GUI module"

from PyQt5.QtCore import QSize, Qt
from PyQt5.QtGui import QDoubleValidator

from PyQt5.QtWidgets import (
    QApplication,
    QCheckBox,
    QDialog,
    QHBoxLayout,
    QLabel,
    QLineEdit,
    QPushButton,
    QTextEdit,
    QVBoxLayout,
    QWidget,
    QMainWindow,
    QComboBox,
    QScrollArea,
)

from tables_lecteur._data import Familles
from tables_lecteur._tableobj import HorsDeLaTableError


class WidFam(QWidget):
    def __init__(self, fam, parent=None):
        super().__init__()
        self._fam = fam
        self._layout = QVBoxLayout(self)
        self._layout.setContentsMargins(5, 5, 5, 5)

        self._widinfogen = QWidget()
        self._widinfogen_layout = QVBoxLayout(self._widinfogen)
        self._lab0 = QLabel("<b><u>Informations générales</u></b>")
        self._widinfogen_layout.addWidget(self._lab0)
        self._lab1 = QLabel(
            f"Table de {fam.resultat.html} en fonction de {fam.variable.html}"
        )
        self._widinfogen_layout.addWidget(self._lab1)
        if fam.resultatalias.html:
            self._lab2 = QLabel(
                f"Notation: {fam.resultat.html} = {fam.resultatalias.html}"
            )
            self._widinfogen_layout.addWidget(self._lab2)
        self._lab_prop = [QLabel("Propriété: " + prop.html) for prop in fam.proprietes]
        for lab in self._lab_prop:
            self._widinfogen_layout.addWidget(lab)
        self._layout.addWidget(self._widinfogen)

        self._sel_params = []
        if fam.parametres:
            self._widparam = QWidget()
            self._widparam_layout = QVBoxLayout(self._widparam)
            self._widparam_lab0 = QLabel("<b><u>Paramètres</u></b>")
            self._widparam_layout.addWidget(self._widparam_lab0)
            for param in fam.parametres:
                lay = QHBoxLayout()
                lab = QLabel(f"{param.nom.html} = ")
                lab.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
                lay.addWidget(lab)
                cbox = QComboBox()
                for x in param.values:
                    cbox.addItem(x.html)
                lay.addWidget(cbox)
                lay.addWidget(QWidget(self._widparam))
                lay.addWidget(QWidget(self._widparam))
                self._sel_params.append(cbox)
                cbox.currentIndexChanged.connect(self._changeparam)
                self._widparam_layout.addLayout(lay)
            self._layout.addWidget(self._widparam)

        self._widaction = QWidget()
        self._widaction_layout = QVBoxLayout(self._widaction)
        self._widaction_lab0 = QLabel("<b><u>Actions</u></b>")
        self._widaction_layout.addWidget(self._widaction_lab0)
        laya1 = QHBoxLayout()
        laba1 = QLabel(f"Lire {fam.resultat.html} pour {fam.variable.html} = ")
        laba1.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        laya1.addWidget(laba1)
        self._lire_var = QLineEdit()
        self._lire_var.setMinimumWidth(50)
        laya1.addWidget(self._lire_var)
        btna1 = QPushButton("Lire")
        self._lire_var.returnPressed.connect(self._lire)
        btna1.clicked.connect(self._lire)
        laya1.addWidget(btna1)
        self._widaction_layout.addLayout(laya1)
        laya2 = QHBoxLayout()
        laba2 = QLabel(f"Chercher {fam.variable.html} pour {fam.resultat.html} = ")
        laba2.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        laya2.addWidget(laba2)
        self._chercher_var = QLineEdit()
        self._chercher_var.setMinimumWidth(50)
        laya2.addWidget(self._chercher_var)
        btna2 = QPushButton("Chercher")
        self._chercher_var.returnPressed.connect(self._chercher)
        btna2.clicked.connect(self._chercher)
        laya2.addWidget(btna2)
        self._widaction_layout.addLayout(laya2)
        self._layout.addWidget(self._widaction)

        self._widres = QWidget()
        self._widres_layout = QVBoxLayout(self._widres)
        self._widres_lab0 = QLabel("<b><u>Résultats</u></b>")
        self._widres_layout.addWidget(self._widres_lab0)
        self._widres_line1 = QLabel()
        self._widres_line2 = QLabel()
        self._widres_layout.addWidget(self._widres_line1)
        self._widres_layout.addWidget(self._widres_line2)

        self._layout.addWidget(self._widres)

        self._changeparam()
        self.setLayout(self._layout)

    def _print_res(self, res):
        self._widres_line1.setText("")
        self._widres_line2.setText("")
        for l, r in zip((self._widres_line1, self._widres_line2), res):
            l.setText(
                f"Pour {self._fam.variable.html}={r[0].html}, on a {self._fam.resultat.html}={r[1].html}"
            )

    def _lire(self):
        text = self._lire_var.text().replace(",", ".")
        try:
            var = float(text)
        except ValueError:
            self._widres_line1.setText("Erreur: la valeur entrée n'est pas un nombre.")
            self._widres_line2.setText("")
            return None
        try:
            res = self._current_table.lire(var)
        except HorsDeLaTableError:
            self._widres_line1.setText(
                "La valeur entrée est hors des bornes de la table."
            )
            self._widres_line2.setText(
                f"{self._fam.variable.html} doit être dans [{self._current_table.xstart.html},{self._current_table.xstop.html}]"
            )
            return None
        self._print_res(res)

    def _chercher(self):
        text = self._chercher_var.text().replace(",", ".")
        try:
            res = float(text)
        except ValueError:
            self._widres_line1.setText("Erreur: la valeur entrée n'est pas un nombre.")
            self._widres_line2.setText("")
            return None
        res = self._current_table.chercher(res)
        self._print_res(res)

    def _changeparam(self):
        idx = tuple(
            fp.values[sp.currentIndex()].val
            for fp, sp in zip(self._fam.parametres, self._sel_params)
        )
        self._current_table = self._fam.tables[idx]
        self._widres_line1.setText("")
        self._widres_line2.setText("")


class WidCentral(QWidget):
    def __init__(self, parent=None):
        super().__init__()
        self._mainLayout = QVBoxLayout(self)
        self._mainLayout.setContentsMargins(5, 5, 5, 5)

        self._size = 20
        laysize = QHBoxLayout()
        self._sizelabel = QLabel()
        laysize.addWidget(self._sizelabel)
        btnm = QPushButton("-")
        btnp = QPushButton("+")
        btnm.setFocusPolicy(Qt.NoFocus)
        btnp.setFocusPolicy(Qt.NoFocus)
        btnm.clicked.connect(self._sizem)
        btnp.clicked.connect(self._sizep)
        laysize.addWidget(btnm)
        laysize.addWidget(btnp)
        self._mainLayout.addLayout(laysize)

        self._select_famille = QComboBox()
        self._select_famille.addItem("Selectionner une table")
        self._select_famille.currentIndexChanged.connect(self._famChanged)
        for fam in Familles:
            self._select_famille.addItem(fam.nom)
        self._mainLayout.addWidget(self._select_famille)
        self._famWidgets = [QWidget()] + [WidFam(fam) for fam in Familles]
        for w in self._famWidgets:
            w.hide()
            self._mainLayout.addWidget(w)
        self._famWidgets[0].show()
        self.setLayout(self._mainLayout)
        self._update_size()

    def _sizem(self):
        if self._size < 1:
            pass
        elif self._size <= 16:
            self._size -= 1
        else:
            self._size -= 2
        self._update_size()

    def _sizep(self):
        if self._size < 16:
            self._size += 1
        else:
            self._size += 2
        self._update_size()

    def _update_size(self):
        self._sizelabel.setText(f"Taille courante: {self._size}px")
        self.setStyleSheet(f"font-size: {self._size}px;")

    def _famChanged(self):
        idx = self._select_famille.currentIndex()
        for w in self._famWidgets:
            w.hide()
        self._famWidgets[idx].show()


# class WidMain(QMainWindow):
class WidMain(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle("Tables statistiques — SY02")
        self._central = QScrollArea()
        self._central.setWidget(WidCentral())
        self._central.setWidgetResizable(True)
        # self.setCentralWidget(self._central)
        self._lay = QVBoxLayout(self)
        self._lay.addWidget(self._central)
        self.setLayout(self._lay)
        self.resize(800, 800)


def get_gui():
    app = QApplication([])
    mwg = WidMain()
    mwg.show()
    app.exec_()
