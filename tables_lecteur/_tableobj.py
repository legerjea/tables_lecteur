# Copyright 2022, Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

"Représentation des tables"

import math
import json
from collections import namedtuple

TableValue = namedtuple("TableValue", ("val", "html", "latex"))
TableParametre = namedtuple("TableParametre", ("nom", "values"))
TableExpr = namedtuple("TableExpr", ("html", "latex"))


class HorsDeLaTableError(Exception):
    pass


VERSION = 0


class FormatVersionError(Exception):
    pass


class ListeFamillesTables(list):
    @classmethod
    def load(cls, stream):
        obj = json.load(stream)
        return cls._load_from_obj(obj)

    @classmethod
    def loads(cls, content):
        obj = json.loads(content)
        return cls._load_from_obj(obj)

    @classmethod
    def _load_from_obj(cls, obj):
        if "version" not in obj or obj["version"] != VERSION:
            raise FormatVersionError
        ret = cls()
        for fam in obj["families"]:
            ret.append(FamilleTables.from_jsonobj(fam))
        return ret

    def dump(self, stream):
        obj = {"version": VERSION, "families": [fam.to_jsonobj() for fam in self]}
        json.dump(obj, stream)


class FamilleTables:
    def __init__(
        self,
        nom=None,
        proprietes=(),
        variable=None,
        resultat=None,
        resultatalias=TableExpr("", ""),
        parametres=None,
        tables=None,
    ):
        self.nom = nom
        self.proprietes = proprietes
        self.variable = variable
        self.resultat = resultat
        self.resultatalias = resultatalias
        self.parametres = parametres
        self.tables = tables

    @classmethod
    def from_jsonobj(cls, _jsonobj):
        return cls(
            nom=_jsonobj["nom"],
            proprietes=tuple(TableExpr(*z) for z in _jsonobj["proprietes"]),
            variable=TableExpr(*_jsonobj["variable"]),
            resultat=TableExpr(*_jsonobj["resultat"]),
            resultatalias=TableExpr(*_jsonobj["resultatalias"]),
            parametres=tuple(
                TableParametre(TableExpr(*p[0]), tuple(TableValue(*z) for z in p[1]))
                for p in _jsonobj["parametres"]
            ),
            tables={tuple(x): Table.from_jsonobj(y) for (x, y) in _jsonobj["tables"]},
        )

    def to_jsonobj(self):
        return {
            "nom": self.nom,
            "proprietes": tuple(tuple(z) for z in self.proprietes),
            "variable": tuple(self.variable),
            "resultat": tuple(self.resultat),
            "resultatalias": tuple(self.resultatalias),
            "parametres": tuple(
                (p.nom, tuple(tuple(z) for z in p.values)) for p in self.parametres
            ),
            "tables": tuple(
                (tuple(x), t.to_jsonobj()) for (x, t) in self.tables.items()
            ),
        }


class Table:
    def __init__(
        self, xstart=None, xstop=None, increasing=True, xvalues=(), yvalues=()
    ):
        self.xstart = xstart
        self.xstop = xstop
        self.increasing = increasing
        self.xvalues = xvalues
        self.yvalues = yvalues
        assert len(xvalues) == len(yvalues)

    @classmethod
    def from_jsonobj(cls, _jsonobj):
        return cls(
            xstart=TableValue(*_jsonobj["xstart"]),
            xstop=TableValue(*_jsonobj["xstop"]),
            increasing=_jsonobj["increasing"],
            xvalues=tuple(TableValue(*z) for z in _jsonobj["xvalues"]),
            yvalues=tuple(TableValue(*z) for z in _jsonobj["yvalues"]),
        )

    def to_jsonobj(self):
        return {
            "xstart": tuple(self.xstart),
            "xstop": tuple(self.xstop),
            "increasing": self.increasing,
            "xvalues": tuple(tuple(z) for z in self.xvalues),
            "yvalues": tuple(tuple(z) for z in self.yvalues),
        }

    def lire(self, x):
        if x > self.xstop.val:
            raise HorsDeLaTableError
        if x < self.xstart.val:
            raise HorsDeLaTableError
        found = [
            (a, b) for a, b in zip(self.xvalues, self.yvalues) if math.isclose(a.val, x)
        ]
        if found:
            return (found[0],)
        found1 = [(a, b) for a, b in zip(self.xvalues, self.yvalues) if a.val < x]
        found2 = [(a, b) for a, b in zip(self.xvalues, self.yvalues) if a.val > x]
        ret = []
        if found1:
            ret.append(found1[-1])
        if found2:
            ret.append(found2[0])
        return tuple(ret)

    def chercher(self, y):
        found = [
            (a, b) for a, b in zip(self.xvalues, self.yvalues) if math.isclose(b.val, y)
        ]
        if found:
            return (found[0],)
        found1 = [(a, b) for a, b in zip(self.xvalues, self.yvalues) if b.val < y]
        found2 = [(a, b) for a, b in zip(self.xvalues, self.yvalues) if b.val > y]
        ret = []
        if self.increasing:
            if found1:
                ret.append(found1[-1])
            if found2:
                ret.append(found2[0])
            return tuple(ret)
        if found2:
            ret.append(found2[-1])
        if found1:
            ret.append(found1[0])
        return tuple(ret)
